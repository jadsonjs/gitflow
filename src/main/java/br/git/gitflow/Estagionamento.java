/*
 * Federal University of Rio Grande do Norte
 * Department of Informatics and Applied Mathematics
 * Collaborative & Automated Software Engineering (CASE) Research Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 * Projeto
 * br.git.gitflow
 * Estagionamento
 * 03/05/21
 */
package br.git.gitflow;

/**
 * TODO
 * Jadson Santos - jadsonjs@gmail.com
 */
public class Estagionamento {

    private int numeroVagas = 10;

    Vaga[] vagas;

    public Estagionamento(final int numeroVagas){
        this.numeroVagas = numeroVagas;
        vagas = new Vaga[numeroVagas];
        for (int i = 0; i< vagas.length; i++){
            vagas[i] = new Vaga();
        }
    }

    public void estacionar(Carro carro) {
        for (int i = 0; i< vagas.length; i++){
            if(vagas[i].isLivre()){
                vagas[i].ocupar(carro);
                System.out.printf("Vaga "+i+" Ocupada pelo carro: "+carro.print());
                break;
            }
        }

    }
}
